<?php

class PdoBridge
{
    private static string $serveur = 'mysql:host=localhost';
    private static string $bdd = 'dbname=annuaire';
    private static string $user = 'root';
    private static string $mdp = '';
    private static string $login = 'login';
    private static $monPdoBridge = null;
    /**
     * @var PDO   <--- need by PhpStorm to find Methods of PDO
     */

    private static PDO $monPdo;

    /**
     * Constructeur privé, crée l'instance de PDO qui sera sollicitée
     * pour toutes les méthodes de la classe
     */
    private function __construct()
    {
        PdoBridge::$monPdo = new PDO(PdoBridge::$serveur . ';' . PdoBridge::$bdd, PdoBridge::$user, PdoBridge::$mdp);
        PdoBridge::$monPdo->query("SET CHARACTER SET utf8");
    }

    public function _destruct()
    {
        PdoBridge::$monPdo = null;
    }

    /**
     * Fonction statique qui crée l'unique instance de la classe
     *
     * Appel : $instancePdolafleur = PdoBridge::getPdoBridge();
     * @return l'unique objet de la classe PdoBridge
     */
    public static function getPdoBridge()
    {
        if (PdoBridge::$monPdoBridge == null) {
            PdoBridge::$monPdoBridge = new PdoBridge();
        }
        return PdoBridge::$monPdoBridge;
    }

    public static function getAdmin($login, $mdp)
    {
        $req = "select * from admin where login='$login' and mdp='$mdp'";
        $rs = PdoBridge::$monPdo->query($req);
        $ligne = $rs->fetch();
        return $ligne;
    } 

    public function getLesMembres()
    {
        // modifiez la requête sql
        $sql = 'SELECT * FROM membres';
        $lesLignes = PdoBridge::$monPdo->query($sql);
        return $lesLignes->fetchALL(PDO::FETCH_ASSOC);
    }

    public function getMaxId()
    {
        // modifiez la requête sql
        $req = "SELECT MAX(id) AS maxi FROM membres";
        $res = PdoBridge::$monPdo->query($req);
        $lesLignes = $res->fetch();
        return 1 + intval($lesLignes["maxi"]);
    }

    public function deleteMembres($id)
    {
        $sql = "DELETE FROM membres WHERE id = '$id'";
        $res = PdoBridge::$monPdo->query($sql);
        $lesLignes = $res->fetch();
     

    }


    public function insertMembre($nom, $prenom)
    {
        // modifiez la requête sql
        $id = $this->getMaxId();
        // modifiez la requête sql
        $sql = "INSERT INTO membres (id, prenom, nom) Values('$id', '$prenom', '$nom')";
        $req = PdoBridge::$monPdo->exec($sql);
    }

    public function deleteMembre($id)
    {
        $sql = "DELETE FROM membres WHERE id = '$id'";
        $req = PdoBridge::$monPdo->exec($sql);
    }

    public function modifierMembre($id,$prenom,$nom)
    {
        $sql = "UPDATE membres SET prenom = '$prenom', nom = '$nom', WHERE id = '$id'";
        $req = PdoBridge::$monPdo->exec($sql);
    }
}