<?php
switch ($action) {
    case 'accueil':
    {
        if(isset ($_SESSION ['login']) )
        {
            $message="Bienvenue au membre";
            include("views/v_accueil.php");
        }
        else
        {
            include("views/v_connexion.php");
        }
        
        break;
    }
    case 'connexion':
        {
    
            $message="Connexion";
            include("views/v_connexion.php");
            break;
        }

    case 'lister': {
        $lesMembres=$pdo->getLesMembres();
        require 'views/v_listemembres.php';
        break;
    }
    case 'saisir':
    {
        require "views/v_saisie.php";
        break;
    }

    case 'modifier':
        {
            
            $lesMembres=$pdo->getLesMembres();
            include("views/v_modifier.php");
            break;
        }

    case 'modifierMembre':    
        {   
            $id = $_REQUEST['id'];
            $nom = $_REQUEST['nom'];
            $prenom = $_REQUEST['prenom'];
            $modifier = $pdo -> modifierMembre($id,$prenom,$nom);
            $lesMembres=$pdo->getLesMembres();
            include("views/v_listemembres.php");
            break;
        }

    case 'controlesaisie':
    {
        $nom = $_REQUEST['nom'];

        $prenom = $_REQUEST['prenom'];
        if (empty($nom) || empty($prenom)) {
            require "views/v_saisie.php";
        } else {
            $resultat = $pdo->insertMembre($nom, $prenom);
            $message="Bienvenue au membre $prenom $nom !";

            include("views/v_accueil.php");
        }
        break;
    }

    case 'supprimer' :
    {
        $id = $_REQUEST ['id'];
        $resultat=$pdo->deleteMembres($id); 
        $lesMembres=$pdo->getLesMembres(); 
        include("views/v_deletemembre.php");
        
        break;
    }
    case 'afficherSuppr' :
    {
        $lesMembres=$pdo->getLesMembres(); 
        include("views/v_deletemembre.php");
        break;
    }
    
    default:
    {
        $_SESSION["message_erreur"] = "Site en construction";
        include("views/404.php");
        break;
    }


}
